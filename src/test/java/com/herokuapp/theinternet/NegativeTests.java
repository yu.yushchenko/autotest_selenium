package com.herokuapp.theinternet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NegativeTests {

	@Test
	public void IncorrectUsernameTest() {

		WebDriver driver = init();

		setUp(driver);

		login(driver, "incorrect", "SuperSecretPassword");

		sleep(3000);

		// verifications
		// 1
		verifyUrl(driver, "http://the-internet.herokuapp.com/login");

		// 2
		verifyButtons(driver);

		// 4
		verify(driver, "Your username is invalid!");

		/*
		 * WebElement failMessage =
		 * driver.findElement(By.xpath("//div[@class='flash error']")); String
		 * expectedFailMessage = "Your username is invalid!"; String actualFailMessage =
		 * failMessage.getText();
		 * 
		 * Assert.assertTrue(actualFailMessage.contains(expectedFailMessage),
		 * "\"Actual Message is not same as Expected.\\nActual Message: \" + actualFailMessage + \"\\n.Expected Message: \"\n"
		 * + " + expectedFailMessage");
		 */

		driver.quit();

	}

	private void verifyButtons(WebDriver driver) {

		// login button
		WebElement loginButton2 = driver.findElement(By.xpath("//form[@id='login']//i[@class='fa fa-2x fa-sign-in']"));
		Assert.assertTrue(loginButton2.isDisplayed(), "Login button is displayed");

		// username button
		WebElement username2 = driver.findElement(By.name("username"));
		Assert.assertTrue(username2.isDisplayed(), "Username is still there");
	}

	private void verifyUrl(WebDriver driver, String url) {

		String expectedurl = url;
		String ActualUrl = driver.getCurrentUrl();

		Assert.assertEquals(ActualUrl, expectedurl, "Actual URL is not the same as Expected");
	}

	@Test

	public void incorrectPasswordTest() {

		WebDriver driver = init();

		setUp(driver);

		login(driver, "tomsmith", "incorrectpassword");

		sleep(3000);

		// verifications

		verify(driver, "Your password is invalid!");

		driver.quit();

	}

	private void verify(WebDriver driver, String message) {

		WebElement errorMessage = driver.findElement(By.xpath("//div[@class='flash error']"));
		String expectedErrorMessage = message;
		String actualErrorMessage = errorMessage.getText();

		Assert.assertTrue(actualErrorMessage.contains(expectedErrorMessage),
				"\"Actual Message is not same as Expected.\\nActual Message: \" + actualErrorMessage + \"\\n.Expected Message: \"\n"
						+ " + expectedErrorMessage");
	}

	private void login(WebDriver driver, String name, String pass) {

		WebElement username = driver.findElement(By.id("username"));
		username.sendKeys(name);

		WebElement password = driver.findElement(By.id("password"));
		password.sendKeys(pass);

		WebElement loginButton = driver.findElement(By.tagName("button"));

		loginButton.click();
	}

	private void setUp(WebDriver driver) {
		driver.manage().window().maximize();

		String url = "http://the-internet.herokuapp.com/login";
		driver.get(url);
	}

	private WebDriver init() {

		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
		WebDriver driver = new ChromeDriver();
		return driver;
	}

	private void sleep(long i) {

		try {
			Thread.sleep(i);

		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
